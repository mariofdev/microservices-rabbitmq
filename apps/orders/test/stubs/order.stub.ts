import { Order } from 'apps/orders/src/schemas/order.schema';
import { Types } from 'mongoose';

export const orderStub = (): Order => {
    return{
        _id: '65423a1cef53003fb1096b09' as unknown as Types.ObjectId,
        name: 'White Tee',
        price: 199.99,
        phoneNumber: '+34673777777'
    }
}