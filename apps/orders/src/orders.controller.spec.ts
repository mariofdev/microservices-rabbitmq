import { Test, TestingModule } from '@nestjs/testing';
import { OrdersController } from './orders.controller';
import { OrdersService } from './orders.service';
import { Order } from './schemas/order.schema';
import { orderStub } from '../test/stubs/order.stub';
import { CreateOrderDto } from './dto/create-order.dto';

jest.mock('./orders.service');

describe('OrdersController', () => {
  let ordersController: OrdersController;
  let ordersService: OrdersService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [OrdersController],
      providers: [OrdersService],
    }).compile();

    ordersController = app.get<OrdersController>(OrdersController);
    ordersService = app.get<OrdersService>(OrdersService);
  });

  describe('createOrder', () => {
    let orderCreated: Order;
    let createOrderDto: CreateOrderDto;

    beforeEach(async () => {
      const { name, price, phoneNumber } = orderStub();

      createOrderDto = {
        name,
        price,
        phoneNumber,
      };

      orderCreated = await ordersController.createOrder(createOrderDto);
    });

    it('should create a new order', async () => {
      expect(orderCreated).toEqual(orderStub());
    });

    it('should call orders service', () => {
      expect(ordersService.createOrder).toHaveBeenCalled();
    });
  });

  describe('getOrders', () => {
    let orders: Order[];

    beforeEach(async () => {
      orders = await ordersService.getOrders();
    });

    it('should return an array of orders', () => {
      expect(orders).toEqual([orderStub()]);
    });

    it('should call orders service', () => {
      expect(ordersService.getOrders).toBeCalled();
    });
  });
});
