import { Test, TestingModule } from '@nestjs/testing';
import { OrdersService } from './orders.service';
import { Order } from './schemas/order.schema';
import { OrdersRepository } from './orders.repository';
import { ClientProxy } from '@nestjs/microservices';
import { createMock } from '@golevelup/ts-jest';
import { orderStub } from '../test/stubs/order.stub';
import { CreateOrderDto } from './dto/create-order.dto';
import { of } from 'rxjs';

jest.mock('./orders.repository');

describe('OrdersService', () => {
  let ordersService: OrdersService;
  let ordersRepository: OrdersRepository;
  let billingClientMock;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [],
      providers: [
        OrdersService,
        OrdersRepository,
        {
          provide: 'BILLING',
          useValue: createMock<ClientProxy>(),
        },
      ],
    }).compile();

    ordersRepository = app.get<OrdersRepository>(OrdersRepository);
    ordersService = app.get<OrdersService>(OrdersService);
    billingClientMock = app.get<ClientProxy>('BILLING');
  });

  it('should be defined', () => {
    expect(ordersService).toBeDefined();
  });

  describe('should crete a new order', () => {
    let orderCreated: Order;
    let createOrderDto: CreateOrderDto;

    beforeEach(async () => {
        billingClientMock.emit.mockReturnValue(of({
            status: 'success',
            message: 'Order created successfully',
          }));

      const { name, price, phoneNumber } = orderStub();

      createOrderDto = {
        name,
        price,
        phoneNumber,
      };

      orderCreated = await ordersService.createOrder(createOrderDto);
    });

    it('should return a new order', () => {
      expect(orderCreated).toEqual(orderStub());
    });
  });
});
