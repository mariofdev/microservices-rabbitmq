import { orderStub } from '../../test/stubs/order.stub';

export const OrdersRepository = jest.fn().mockReturnValue({
    create: jest.fn().mockResolvedValue(orderStub()),
    find: jest.fn(),
    startTransaction: jest.fn().mockReturnValue({
      commitTransaction: jest.fn(),
      abortTransaction: jest.fn(),
    }),
  });
