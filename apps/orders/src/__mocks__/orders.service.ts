import { orderStub } from '../../test/stubs/order.stub';

export const OrdersService = jest.fn().mockReturnValue({
  createOrder: jest.fn().mockResolvedValue(orderStub()),
  getOrders: jest.fn().mockResolvedValue([orderStub()]),
});
